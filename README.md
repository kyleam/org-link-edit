Org Link Edit provides Paredit-inspired slurping and barfing commands
for Org link descriptions.

The commentary in [org-link-edit.el][el] contains more details.
org-link-edit.el is available in Org's contrib directory.  See the Org
manual's [installation instructions][install] for information on using
contributed packages.

[el]: https://git.sr.ht/~kyleam/org-link-edit/tree/master/org-link-edit.el#L24
[install]: http://orgmode.org/org.html#Installation
